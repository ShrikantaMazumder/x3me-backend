const pjson = require("../../dependencies/nodejs/package.json");

exports.lambdaHandler = async (event) => {
  let response = {};
  try {
    response = {
      statusCode: 200,
      body: JSON.stringify({
        // name: pjson.name,
        version: pjson.version,
        name: "Shrikanta",
      }),
    };
  } catch (err) {
    console.log('error')
    console.log(err);
    return err;
  }

  return response;
};
